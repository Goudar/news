from Article import Article
from urllib.request import urlopen
from pymongo import errors as pyErrors
from pymongo import MongoClient
from time import sleep
# library to parse HTML
from bs4 import BeautifulSoup as bs
import hashlib
import re
import json
import requests as myRequest

continueScrape = True
class InShortsScraper(object):
    firstLoad = True
    newsContent = []
    newsOffset = None

    def __init__(self):
        # print("Creating the InShortsScraper object.")
        self.loadMore = {
            "category": "politics",
            "news_offset": None
        }
        self.articlesInPage = []
        self.articlesObjectList = []
        self.pattern = None

    def scrapePage(self):
        if len(InShortsScraper.newsContent) != 0:
            InShortsScraper.firstLoad = False
        
        # print("isFirstLoad : " + str(InShortsScraper.firstLoad))
        if InShortsScraper.firstLoad == True:
            # print("First Load")
            url = "https://inshorts.com/en/read/politics"
            # print("Offset : " + str(InShortsScraper.newsOffset))
            self.articlesInPage = self.scrapeArticles(url, InShortsScraper.newsOffset)
            InShortsScraper.newsContent.append(self.articlesInPage)
        else:
            url = "https://inshorts.com/en/ajax/more_news"
            # print("Offset : " + str(InShortsScraper.newsOffset))
            self.articlesInPage = self.scrapeArticles(url, InShortsScraper.newsOffset)
        
        for nArticle in self.articlesInPage:
            title = nArticle.find('a').span.string
            author = nArticle.find('span', class_="author").string
            timePublished = nArticle.find('span', class_="time").string
            date = nArticle.find(attrs={"clas": "date"}).string
            bodyDiv = nArticle.find('div', class_="news-card-content")
            body = bodyDiv.div.string
            link = nArticle.find('a', class_="source").get('href')
            _id = hashlib.md5(str(title).encode())
            self.articlesObjectList.append(Article(_id.hexdigest(),title, body,author, date, link, timePublished))

        
        self.insertToDatabase(self.articlesObjectList)


    def scrapeArticles(self, url, news_offset = None):
        if InShortsScraper.firstLoad == True:
            self.pattern = re.compile('var min_news_id = (.+?);')
        else:
            self.pattern = re.compile('min_news_id = (.+?);')
        page = None
        # print("Pattern: " + str(self.pattern))
        if news_offset == None:
            htmlPage = urlopen(url)
            page = bs(htmlPage, "html.parser")
        else:
            self.loadMore['news_offset'] = InShortsScraper.newsOffset
            # print("payload : " + str(self.loadMore))
            try:
                r = myRequest.post(
                    url = url,
                    data = self.loadMore
                )
            except TypeError:
                print("Error in loading")
            
            InShortsScraper.newsOffset = r.json()["min_news_id"]
            page = bs(r.json()["html"], "html.parser")
        #print(page)
        if InShortsScraper.newsOffset == None:
            scripts = page.find_all("script")
            for script in scripts:
                for line in script:
                    scriptString = str(line)
                    if "min_news_id" in scriptString:
                        finder = re.findall(self.pattern, scriptString)
                        InShortsScraper.newsOffset = finder[0].replace('min_news_id = ', '').replace('"','').replace(';','').strip()
        
        
        newsArticles = page.find_all("div", class_="news-card")
        return newsArticles

    def insertToDatabase(self,newsArticlesList):
        dbClient = MongoClient('10.10.13.225')
        dbInshorts = dbClient.inShorts
        articleCollection = dbInshorts['articles']
        global continueScrape
        for article in newsArticlesList:
            count = 0
            try:
                articleCollection.insert(article.getDictionary())
                count += 1
            except pyErrors.DuplicateKeyError:
                continue
        if count == 0:
            continueScrape = False
        print("Inserted " + str(count) + " articles")

    
if __name__ == '__main__':
    scraper = InShortsScraper()
    while continueScrape != False:
        scraper.scrapePage()