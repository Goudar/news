class Article:
    def __init__(self, _id, title,body, author, publishedOn, link, time):
        self._id = _id
        self.title = title
        self.publishedOn = publishedOn
        self.body = body
        self.author = author
        self.link = link
        self.time = time
        self.myDictionary = {}
    
    def getDictionary(self):
        self.myDictionary['_id'] = self._id
        self.myDictionary['title'] = self.title
        self.myDictionary['body'] = self.body
        self.myDictionary['publishedOn'] = self.publishedOn
        self.myDictionary['author'] = self.author
        self.myDictionary['link'] = self.link
        self.myDictionary['publishedTime'] = self.time
        return self.myDictionary